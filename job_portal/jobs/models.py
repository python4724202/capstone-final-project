from django.db import models
from django.contrib.auth.models import User

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    is_employer = models.BooleanField(default=False)

class Order(models.Model):
    title = models.CharField(max_length=100)
    description = models.TextField()
    price = models.DecimalField(max_digits=10, decimal_places=2)
    employer = models.ForeignKey(User, on_delete=models.CASCADE, related_name='posted_orders')
    worker = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True, related_name='accepted_orders')
    completed = models.BooleanField(default=False)

class Review(models.Model):
    author = models.ForeignKey(User, on_delete=models.CASCADE, related_name='authored_reviews')
    target = models.ForeignKey(User, on_delete=models.CASCADE, related_name='received_reviews')
    text = models.TextField()
    rating = models.IntegerField()