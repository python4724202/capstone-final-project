from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.decorators import login_required
from .models import Profile, Order
from .forms import UserRegistrationForm 
from django.contrib.auth.models import User

@login_required
def home(request):
    return render(request, 'home.html')

def login_view(request):
    if request.method == 'POST':
        form = AuthenticationForm(request, request.POST)
        if form.is_valid():
            # Получаем пользователя из формы аутентификации
            user = form.get_user()

            # Проверяем, является ли пользователь работодателем
            if hasattr(user, 'profile') and user.profile.is_employer:
                # Перенаправляем работодателя на страницу post_order
                return redirect('post_order')
            else:
                # Перенаправляем работника на страницу выбора работ
                return redirect('choose_worker')
    else:
        form = AuthenticationForm()
    return render(request, 'login.html', {'form': form})

def register_choice(request):
    if request.method == 'POST':
        if request.POST.get('role') == 'employer':
            return redirect('register_employer')
        elif request.POST.get('role') == 'worker':
            return redirect('register_worker')
    return render(request, 'register_choice.html')

def register_employer(request):
    if request.method == 'POST':
        form = UserRegistrationForm(request.POST)
        if form.is_valid():
            user = form.save()
            Profile.objects.create(user=user, is_employer=True)
            return redirect('post_order')  # Перенаправление на страницу post_order
    else:
        form = UserRegistrationForm()
    return render(request, 'register_employer.html', {'form': form})

def register_worker(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            user = form.save()
            # Создание профиля и установка флага работника
            profile = Profile.objects.create(user=user, is_employer=False)
            return redirect('choose_worker')  # Перенаправление на страницу выбора работ
    else:
        form = UserCreationForm()
    return render(request, 'register_worker.html', {'form': form})


@login_required
def post_order(request):
    if request.method == 'POST':
        title = request.POST.get('title')
        description = request.POST.get('description')
        price = request.POST.get('price')
        
        # Проверяем, что пользователь аутентифицирован
        if request.user.is_authenticated:
            employer = request.user  # Получаем текущего аутентифицированного пользователя
            
            # Создаем заказ
            Order.objects.create(title=title, description=description, price=price, employer=employer)

            # Перенаправляем на страницу со списком заказов
            return redirect('order_list')
    return render(request, 'choose_worker.html')
    
def order_list(request):
    # Получаем список всех заказов текущего пользователя
    orders = Order.objects.filter(employer=request.user)
    return render(request, 'order_list.html', {'orders': orders})

def choose_worker(request, order_id):
    order = Order.objects.get(id=order_id)
    if request.method == 'POST':
        worker_id = request.POST.get('worker')
        order.worker_id = worker_id
        order.save()
        return redirect('home')
    workers = Profile.objects.filter(is_employer=False)
    return render(request, 'choose_worker.html', {'order': order, 'workers': workers})